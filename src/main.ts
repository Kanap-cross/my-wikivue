import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createI18n } from "vue-i18n";

import './assets/main.css'

// Get user language by browser
const userLang = navigator.language;

const i18n = createI18n({
  // something vue-i18n options here ...
    legacy: false,
    locale: navigator.language,
    messages: {
        en: {
            hello: "Hello!",
            test: "english test"
        },
        fr: {
            hello: "Bonjour !",
            test: "test français"
        },
        ja: {
            hello: "こんにちは！",
            test: "Yamete test"
        },
    },
});
const messages = {
  en: {
    message: {
      hello: "{msg} world",
    },
  },
};



const app = createApp(App);

app.use(router);
app.use(i18n);

app.mount("#app");