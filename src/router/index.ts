import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import DataTable from '@/views/DataTable.vue'
import RangeSlider from '@/views/RangeSlider.vue'
import ComponentNavigation from '@/views/componentNavigation.vue'
import Dropdown from '@/views/Dropdown.vue'
import DragNDrop from '@/views/DragNDrop.vue'
import ParentToChildSync from "@/views/ParentToChildSync.vue"
import Traductions from '@/views/Traductions.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
      meta: {
        breadCrumb: "home",
      },
    },
    {
      path: "/data-table",
      name: "data-table",
      component: DataTable,
      meta: {
        breadCrumb: "data-table",
      },
    },
    {
      path: "/slider",
      name: "slider",
      component: RangeSlider,
      meta: {
        breadCrumb: "slider",
      },
    },
    {
      path: "/navigation",
      name: "navigation",
      component: ComponentNavigation,
      meta: {
        breadCrumb: "navigation",
      },
    },
    {
      path: "/dropdown",
      name: "dropdown",
      component: Dropdown,
      meta: {
        breadCrumb: "dropdown",
      },
    },
    {
      path: "/dragNDrop",
      name: "drag-and-drop",
      component: DragNDrop,
      meta: {
        breadCrumb: "drag-and-drop",
      },
    },
    {
      path: "/pToCSync",
      name: "parent-child-sync",
      component: ParentToChildSync,
      meta: {
        breadCrumb: "parent-child-sync",
      },
    },
    {
      path: "/traductions",
      name: "traductions",
      component: Traductions,
      meta: {
        breadCrumb: "traductions",
      },
    },
  ],
});

export default router
